import './Home.css';
import {Container, Carousel, Row, Col} from 'react-bootstrap';
import greeting from '../images/home.jpg';
import poolA from '../images/pic1.jpg';
import poolB from '../images/pic2.jpg';
import poolC from '../images/pic3.jpg';
import proposal from '../images/proposal.jpg'
import food from '../images/food.jpeg'

export default function Home(){
    return (
        <>
            <div id="header">
                <img
                alt="hehe"
                src={greeting}
                style={{height: '88vh', width: "100%", objectFit: 'cover'}}
                />
            </div>
            {/* Intro-text */}
            <Container className="mt-3">   
            <h2>Palm Garden Resort</h2>
            <hr id="divider"></hr>
            <div className="text-center">
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eveniet incidunt magni, magnam numquam nam veritatis quisquam, nostrum dolorum, vero excepturi ex praesentium unde a dignissimos repellendus quam cumque porro illo.</p>
            </div>

            <Container className="event">
                <Row className="justify-content-md-center">
                    <Col md="auto">
                        <img 
                            className="img"
                            alt="PropPic"
                            src={proposal}
                        />
                    </Col>
                    <Col xs="5" className="event-content">
                        <h3 className="event-title">PLAN YOUR EVENTS</h3>
                        <p className="event-text pt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime est vero exercitationem, repellat fugit commodi minima perferendis aliquid. Beatae nisi inventore at accusamus assumenda vitae labore, odit voluptas modi aspernatur?</p>
                    </Col>
                </Row>
                <Row className="justify-content-md-center mt-5 foodie">
                    <Col xs="5" className="event-content">
                        <h3 className="event-title">PLAN YOUR EVENTS</h3>
                        <p className="event-text pt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime est vero exercitationem, repellat fugit commodi minima perferendis aliquid. Beatae nisi inventore at accusamus assumenda vitae labore, odit voluptas modi aspernatur?</p>
                    </Col>
                    <Col md="auto">
                        <img 
                            className="img"
                            alt="Second Accommodation"
                            src={food}
                        />
                    </Col>
                </Row>
            </Container>
            
            <Carousel className="attractions mt-5">
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={poolA}
                    alt="First slide"
                    />
                    <Carousel.Caption>
                    <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={poolB}
                    alt="Second slide"
                    />

                    <Carousel.Caption>
                    <h3>Second slide label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={poolC}
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    <h3>Third slide label</h3>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
            </Container>
        </>
    )
}